#!/usr/bin/env python3

import rospy

from sim.components.neat import NeatCar

class Murphy(NeatCar):

    def __init__(
            self,
            i,
            position,
            world,
            genome,
            nn,
            debug,
    ):
        super().__init__(self, i, position, world, genome, nn)

        self.debug = debug

        # self.pub_servo = rospy.Publisher('/servo', Int16, queue_size=1)
        # self.pub_esc = rospy.Publisher('/esc', Int16, queue_size=1)

        #self.joy_msg = None
        #rospy.Publisher('/joy', Joy, self._joy_cb, queue_size=1)

        # rospy.Subscriber('/range_1', Range, self._range_1, queue_size=1)
        # rospy.Subscriber('/range_2', Range, self._range_2, queue_size=1)
        # rospy.Subscriber('/range_3', Range, self._range_3, queue_size=1)
        # rospy.Subscriber('/range_4', Range, self._range_4, queue_size=1)
        # rospy.Subscriber('/range_5', Range, self._range_5, queue_size=1)
        # rospy.Subscriber('/range_6', Range, self._range_6, queue_size=1)
        # rospy.Subscriber('/range_7', Range, self._range_7, queue_size=1)
        # rospy.Subscriber('/range_8', Range, self._range_8, queue_size=1)

        rospy.init_node('Murphy')
        self.rate = rospy.Rate(30)

    def _range_1(self, msg):
        self.distances[0] = msg.range
    def _range_2(self, msg):
        self.distances[1] = msg.range
    def _range_3(self, msg):
        self.distances[2] = msg.range
    def _range_4(self, msg):
        self.distances[3] = msg.range
    def _range_5(self, msg):
        self.distances[4] = msg.range
    def _range_6(self, msg):
        self.distances[5] = msg.range
    def _range_7(self, msg):
        self.distances[6] = msg.range
    def _range_8(self, msg):
        self.distances[7] = msg.range

    def spin(self):
        while not rospy.is_shutdown():
            pass


if __name__ == '__main__':
    debug = rospy.get_param('/debug')

    test = Murphy(
            debug,
    )

    test.spin()
