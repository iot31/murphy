import math
import random
from collections import deque

import pygame

from .pos import Pos
from .sensor import Distance
from config import Config


class Car(pygame.sprite.Sprite):
    SENSOR_ANGLES = [
        math.pi,
        math.pi/2,
        0.7,
        0.26,
        0,
        -0.26,
        -0.7,
        -math.pi/2
    ]
    WD = 'RWD'
    MASS = 2.635
    FRONT_LENGTH = 0.13
    REAR_LENGTH = 0.18
    WIDTH = 0.2
    MAX_SPEED = 5
    MAX_ANG = math.pi/6
    MIN_ACC = -5
    MAX_ACC = 6
    B = 1.0 # stiffness
    C = 1.4 # shape
    D = 75 # peak

    def __init__(self, i, position, world):
        pygame.sprite.Sprite.__init__(self)
        self.id = i
        self.destroy = None
        self.finished = False
        self.world = world
        self.road = world.road.segments.copy()
        self.environment = deque()
        self.distance_array = [Distance(self, angle) for angle in self.SENSOR_ANGLES]
        self.distances = None
        self.start_time = world._time
        self.cog = Pos(position)
        self.yaw = 0
        self.yaw_rate = 0
        #self.delta = 0
        self.front_slip = 0
        self.rear_slip = 0
        self.vel = {'x': 0, 'y': 0}
        self.speed = 0
        self.commands = {'steer': 0.0, 'speed': 0.0}
        self.img = pygame.transform.rotate(
            pygame.transform.scale(
                pygame.image.load(
                    './imgs/car.png'
                ).convert_alpha(),
                (
                    int(955 * 0.5),
                    int(551 * 0.5)
                )
            ),
            -180
        )
        self.exploded = False
        self.explosion = pygame.image.load(
            './imgs/explosion.png'
        )
        self.explosion = pygame.transform.scale(
            self.explosion,
            (
                int(self.explosion.get_width()*0.5),
                int(self.explosion.get_height()*0.5),
             )
        )

        # adding a number to the car
        font = pygame.font.SysFont('bahnschrift', 50, bold=True)
        text = pygame.transform.rotate(font.render(str(i), True, Config.BLACK), -90)
        self.img.blit(text, (self.img.get_width()*0.36, self.img.get_height()*(0.50-0.05*len(str(i)))))

        while len(self.environment)<3:
            self.shift_segments()

    def measure(self):
        self.distances = [sensor.measure() for sensor in self.distance_array]

    def listen(self):
        self.commands = {'speed': 0.0, 'steer': 0.0}

    def move(self):

        # moving the car
        #self.acc = FRICTION
        dt = 1 / Config.FPS

        # calculate parallel force
        force_total_par = self.MASS * max(self.MIN_ACC, min(self.MAX_ACC, (self.commands.get('speed') * self.MAX_SPEED - self.speed) / dt))
        if self.WD == 'AWD':
            force_rear_par = force_front_par = force_total_par/2
        elif self.WD == 'RWD':
            force_rear_par = force_total_par
            force_front_par = 0
        elif self.WD == 'FWD':
            force_rear_par = 0
            force_front_par = force_total_par

        # calculate slipping angles
        if self.vel.get('x') < 0:
            rear_slip = -math.atan((self.yaw_rate*self.REAR_LENGTH-self.vel.get('y'))/self.vel.get('x'))
            front_slip = math.atan((self.yaw_rate*self.FRONT_LENGTH+self.vel.get('y'))/self.vel.get('x')) + self.commands.get('steer')
        elif self.vel.get('x') > 0:
            rear_slip = math.atan((self.yaw_rate*self.REAR_LENGTH-self.vel.get('y'))/self.vel.get('x'))
            front_slip = -math.atan((self.yaw_rate*self.FRONT_LENGTH+self.vel.get('y'))/self.vel.get('x')) + self.commands.get('steer')
        else:
            rear_slip = front_slip = 0

        # calculate perpendicular forces (friction)
        force_rear_perp = self.D*math.sin(self.C*math.atan(self.B*rear_slip))
        force_front_perp = self.D*math.sin(self.C*math.atan(self.B*front_slip))

        # calculate change in speeds and yaw
        a_par = (force_rear_par - force_front_perp*math.sin(self.commands.get('steer')) + self.MASS*self.vel.get('y')*self.yaw_rate)/self.MASS
        a_perp = (force_rear_perp - force_front_par*math.cos(self.commands.get('steer')) - self.MASS*self.vel.get('x')*self.yaw_rate)/self.MASS
        inertia_z = self.MASS*((self.REAR_LENGTH+self.FRONT_LENGTH)**2 + self.WIDTH**2)/12
        a_yaw = (force_front_perp*self.FRONT_LENGTH*math.cos(self.commands.get('steer')) - force_rear_perp*self.REAR_LENGTH)/inertia_z

        # adjust speed and yaw_rate
        self.vel['x'] += a_par*dt
        self.vel['y'] += a_perp*dt
        self.yaw_rate += a_yaw*dt
        self.speed = math.copysign(math.sqrt(self.vel.get('x')**2 + self.vel.get('y')**2), self.vel.get('x'))

        if self.speed > 100:
            self.g.fitness = -200
            self.explode()

        # actually move the car
        self.cog.x += dt*(self.vel.get('x')*math.cos(self.yaw) + self.vel.get('y')*math.cos(self.yaw+math.pi/2))*1000
        self.cog.y -= dt*(self.vel.get('x')*math.sin(self.yaw) + self.vel.get('y')*math.sin(self.yaw+math.pi/2))*1000
        self.yaw += self.yaw_rate*dt

        if min(self.distances) < 100:
            self.explode()

        if not self.finished and self.world.road.finish:
            if self.world.road.finish.crossed(self.cog):
                self.world.add_to_leaderboard(self)
                self.destroy = 10
                self.finished = True

    def reward(self):
        return

    def shift_segments(self):
        try:
            self.environment.append(self.road.popleft())
        except IndexError:
            pass
        while len(self.environment) > 3:
            self.environment.popleft()

    def explode(self):
        self.destroy = 10
        self.exploded = True
        self.img = self.explosion

    def update(self):

        if len(self.world._leaderboard)>=3 and ((self.world._time - self.start_time) > max([car[1] for car in self.world._leaderboard[:3]], default=9999999)):
            self.remove(self.world.cars)

        # if (self.world._time - self.start_time) > 1000 and self.vel < 1000:
        #     self.remove(self.world.cars)

        if self.destroy:
            self.destroy -= 1
            if self.destroy == 0:
                self.remove(self.world.cars)

        if not self.exploded:

            # measure the sensors
            self.measure()

            # listen to commands
            self.listen()

            # move the car based on its NN
            self.move()

            # reward the car
            self.reward()

        # prep the image
        self.image = pygame.transform.rotate(
            pygame.transform.scale(
                self.img,
                (
                    int(self.img.get_width() * self.world.scale),
                    int(self.img.get_height() * self.world.scale)
                )
            ),
            self.yaw * 180 / math.pi
        )
        self.rect = self.image.get_rect(center=self.world.screen_coordinates(self.cog))

