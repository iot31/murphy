import math
import random
from collections import deque

import pygame

from sim.components import Car
from .pos import Pos
from .sensor import Distance
from config import Config


class NeatCar(Car):

    SCORE_TRAVEL_MUL = 0.01
    SCORE_ROAD_MUL = 1
    SCORE_STEER_MUL = 1
    SCORE_SPEED_MUL = 0.3
    SCORE_FINISH_MUL = 500
    SCORE_CRASH_MUL = -200

    def __init__(self, i, position, world, genome, nn):
        super().__init__(i, position, world)
        self.id = i
        self.g = genome
        self.fitness = {
            'travel': 0,
            'road': 0,
            'steer': 0,
            'speed': 0,
            'finish': 0,
            'crash': 0,
        }
        self.g.fitness = 0
        self.nn = nn

    def listen(self):

        commands = self.nn.activate(tuple([dist/Distance.MAX_DISTANCE for dist in self.distances] + [self.speed/self.MAX_SPEED, self.commands['steer']/self.MAX_ANG]))
        self.commands['speed'] = min(1, max(0.1, commands[0])) * self.MAX_SPEED
        self.commands['steer'] = min(1, max(-1, commands[1])) * self.MAX_ANG

    def reward(self):

        # self.fitness['travel'] += travel * self.SCORE_TRAVEL_MUL * self.vel/self.MAX_VEL
        # #self.fitness['road'] += (1-(max(self.distances[1], self.distances[7]) - self.world.road.road_width)/800) * self.SCORE_ROAD_MUL
        # self.fitness['steer'] += (1 - abs(prev_steer - self.delta) / 2) * self.SCORE_STEER_MUL
        # #self.fitness['speed'] += self.vel * self.SCORE_SPEED_MUL
        # self.fitness['finish'] = int(self.finished)*self.SCORE_FINISH_MUL
        # self.fitness['crash'] = int(self.exploded)*self.SCORE_CRASH_MUL

        # self.fitness['road'] += (1 - (max(self.distances[1], self.distances[7]) - self.world.road.road_width) / 800) * self.SCORE_ROAD_MUL
        self.fitness['speed'] += self.speed * self.SCORE_SPEED_MUL
        # self.fitness['finish'] = int(self.finished)*self.SCORE_FINISH_MUL
        self.fitness['crash'] = int(self.exploded) * self.SCORE_CRASH_MUL
        fitness = [value for key, value in self.fitness.items()]
        self.g.fitness = sum(fitness) / len(fitness)