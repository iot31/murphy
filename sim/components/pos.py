

class Pos:

    def __init__(self, position):
        self._x = position[0]
        self._y = position[1]

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, x):
        self._x = x

    @property
    def y(self):
        return self._y

    @y.setter
    def y(self, y):
        self._y = y

    def tup(self):
        return self.x, self.y

    def distance(self, pos2):
        return ((pos2.x-self.x)**2 + (pos2.y-self.y)**2)**0.5

    def __str__(self):
        return f"({self.x}, {self.y})"