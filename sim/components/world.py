import pygame
import numpy as np

from .pos import Pos

positions = ['1st', '2nd', '3rd']
colors = [(255, 215, 0), (192, 192, 192), (80, 50, 20)]


class World:

    tick = 0
    #SCALE = 0.01

    def __init__(self, dimensions, scale):
        self.dimensions = dimensions
        self.display = pygame.display.set_mode(self.dimensions)
        self.background = pygame.Surface(self.dimensions)
        self.clock = pygame.time.Clock()
        self._time = 0
        self._highscore = 0
        self.bestcar = None
        self.position = Pos((0, 0))              # the current position we are focussing on
        self.focus = Pos((self.dimensions[0]/2, self.dimensions[1]/2)) # on which point of the display do we want to focus
        self.scale = scale
        self._road = None
        self._cars = pygame.sprite.Group()
        self.font = pygame.font.SysFont('bahnschrift', 50, bold=True)

        self._leaderboard = []

    def screen_coordinates(self, point):
        return (
            int((point.x-self.position.x)*self.scale + self.focus.x),
            int((point.y-self.position.y)*self.scale + self.focus.y)
        )
            # int(list + self.focus[0] - self.bestCarPos[0]),
            # int(list + self.focus[1] - self.bestCarPos[1])
        # )

    @property
    def road(self):
        return self._road

    @road.setter
    def road(self, road):
        self._road = road

    @property
    def cars(self):
        return self._cars

    @cars.setter
    def cars(self, car):
        self._cars = car

    @property
    def highscore(self):
        return self._highscore

    @highscore.setter
    def highscore(self, highscore):
        self._highscore = highscore

    def reset_time(self):
        self._time = 0

    def add_to_leaderboard(self, car):
        self._leaderboard.append((car, self._time - car.start_time))
        self._leaderboard = sorted(self._leaderboard, key=lambda tup: tup[1])

    def follow_best_car(self):
        fitnesses = [car.g.fitness * int( not car.exploded) for car in self.cars]
        if len(fitnesses):
            ind = np.argmax(fitnesses)

            self.bestcar = self.cars.sprites()[ind]
            self.highscore = self.bestcar.g.fitness
            self.position.x = self.position.x * 0.8 + 0.2 * self.bestcar.cog.x
            self.position.y = self.position.y * 0.8 + 0.2 * self.bestcar.cog.y

    def update(self):

        self._time += self.clock.get_time()
        self.display.blit(self.background, (0, 0))

        self.cars.update()

        self.follow_best_car()

        self.road.draw()
        self.cars.draw(self.display)
        # self.road.finish.draw(self.display)

        # for car in self._cars:
        #     car.draw()

        # self.display.blit(
        #     pygame.transform.scale(
        #         self.surface,
        #         (int(self.dimensions[0]*self.scale), int(self.dimensions[1]*self.scale))
        #     ),
        #     (int(self.dimensions[0]*(1-self.scale)/2), int(self.dimensions[1]*(1-self.scale)/2))
        # )

        if self.bestcar:
            font = pygame.font.SysFont('bahnschrift', 50, bold=True)
            text_to_show = font.render(f'{round(self.bestcar.speed/1000*3600,2)} km/h', True, pygame.Color('white'))
            self.display.blit(text_to_show, (0,0))

            for idx, (key, fitness) in enumerate(self.bestcar.fitness.items()):
                font = pygame.font.SysFont('bahnschrift', 20, bold=False)
                text = font.render(f'{key}: {round(fitness,2)}', True, pygame.Color('white'))
                self.display.blit(text, (0, self.display.get_height()-(idx+1)*20))

        for idx, (winner, time) in enumerate(self._leaderboard[:3]):

            text = self.font.render(f' {positions[idx]}: {winner.id} - {time/1000}s', True, colors[idx])
            text_rect = text.get_rect()
            text_rect.right = self.display.get_width()*0.99
            text_rect.top = 10+idx*self.display.get_height()*0.1
            self.display.blit(text, text_rect)


        #pygame.image.save(self.display, f"./screenshots/screen_{self.tick:04d}.jpg")

        pygame.display.update()
