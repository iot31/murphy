#!/usr/bin/env python3
import time
import os

import pygame
import rospy
from std_msgs.msg import Int16
from sensor_msgs.msg import Range, Joy

from sim.components import World, Road, Car

scale = 0.1

class Murphy(Car):

    def __init__(
            self,
            debug,
    ):
        super().__init__(None, None, None)
        self.debug = debug

        # self.pub_servo = rospy.Publisher('/servo', Int16, queue_size=1)
        # self.pub_esc = rospy.Publisher('/esc', Int16, queue_size=1)

        self.distances = [0.0 for i in range(8)]

        #self.joy_msg = None
        #rospy.Publisher('/joy', Joy, self._joy_cb, queue_size=1)

        rospy.Subscriber('/range_1', Range, self._range_1, queue_size=1)
        rospy.Subscriber('/range_2', Range, self._range_2, queue_size=1)
        rospy.Subscriber('/range_3', Range, self._range_3, queue_size=1)
        rospy.Subscriber('/range_4', Range, self._range_4, queue_size=1)
        rospy.Subscriber('/range_5', Range, self._range_5, queue_size=1)
        rospy.Subscriber('/range_6', Range, self._range_6, queue_size=1)
        rospy.Subscriber('/range_7', Range, self._range_7, queue_size=1)
        rospy.Subscriber('/range_8', Range, self._range_8, queue_size=1)

        rospy.init_node('Murphy')
        self.rate = rospy.Rate(30)

    def _range_1(self, msg):
        self.distances[0] = msg.range
    def _range_2(self, msg):
        self.distances[1] = msg.range
    def _range_3(self, msg):
        self.distances[2] = msg.range
    def _range_4(self, msg):
        self.distances[3] = msg.range
    def _range_5(self, msg):
        self.distances[4] = msg.range
    def _range_6(self, msg):
        self.distances[5] = msg.range
    def _range_7(self, msg):
        self.distances[6] = msg.range
    def _range_8(self, msg):
        self.distances[7] = msg.range

    def spin(self):
        while not rospy.is_shutdown():
            pass

    def load(self, path_to_pickle='./models/test.p'):
        rospy.loginfo(os.listdir('.'))

    def simulate(self):
        global scale

        self.id = 0
        self.pos = (0, 0)
        self.world = World((600, 750), scale)
        self.world.road = Road(self.world)

        self.world.scale = 0.01
        self.world.update()
        n_tracks = 10
        for i in range(n_tracks):
            time.sleep(2/n_tracks)
            self.world.road.add_segment()
            self.world.update()

        self.world.road.add_finish()
        self.world.update()

        while self.world.scale < 0.1:
            time.sleep(0.01)
            self.world.scale *=1.05
            self.world.update()

        self.world.cars.add(
            self
            # NeatCar(
            #     i,
            #     (0, 0,),
            #     world,
            #     #g,
            #     #neat.nn.FeedForwardNetwork.create(g, config),
            # )
        )

        t = 0
        run = True
        self.world.reset_time()
        while run:
            t += 1
            self.world.clock.tick(30)
            #world.updateScore(0)

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    run = False
                    pygame.quit()
                    quit()

                if event.type == pygame.MOUSEBUTTONDOWN:
                    if event.button == 4:
                        scale = self.world.scale = self.world.scale*1.1
                    elif event.button == 5:
                        scale = self.world.scale = self.world.scale*0.9

            if len(self.world.cars) == 0:
                run = False
                break

            self.world.update()


if __name__ == '__main__':
    debug = rospy.get_param('/debug')

    test = Murphy(
            debug,
    )

    test.spin()
